#! venv/bin/python3.4
# -*- coding: utf-8 -*-

import os
from flask_script import Manager, Shell, Server
from flask.ext.assets import ManageAssets
from app.assets import assets

from app.app import create_app
from app.settings import DevConfig, ProdConfig
from app.or_admin.models import User

if os.environ.get("APP_ENV") == 'prod':
	app = create_app(ProdConfig)
else:
	app = create_app(DevConfig)

HERE = os.path.abspath(os.path.dirname(__file__))

manager = Manager(app)


def _make_context():
	"""Return context dict for a shell session so you can access
	app, db, and the User model by default.
	"""
	return {'app': app, "User": User}


manager.add_command('server', Server())
manager.add_command('shell', Shell(make_context=_make_context))
manager.add_command('assets', ManageAssets(assets))

if __name__ == '__main__':
	manager.run()