# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_mail import Message
from app.public.forms import ContactForm
from app.public.models import Post, Contacts
from app.utils import flash_errors
from app.extensions import csrf, mail

blueprint = Blueprint("public", __name__, static_folder="../static")


@blueprint.route("/")
def home():
	tag = request.args.get('tag', '')
	if tag:
		posts = Post.objects(tags=tag)
	else:
		posts = Post.objects.all()
	return render_template("index.html", posts=posts)


@csrf.exempt
@blueprint.route("/contact/", methods=["GET", "POST"])
def contactme():
	form = ContactForm(request.form)
	if request.method == 'POST':
		if form.validate_on_submit():
			msg = Message()
			msg.html = '<b>From:</b> %s (%s)<br><b>Message:</b> %s' % (form.name.data,
																  	   form.email.data,
																       form.message.data)
			msg.sender = 'noreply@olegrooz.com'
			msg.recipients = ['mail@olegrooz.com']
			msg.subject = form.subject.data
			mail.send(msg)
			flash('Message has been send', 'success')
		return redirect(url_for('public.contactme'))
	else:
		flash_errors(form)
	contact = Contacts.objects.first_or_404()
	return render_template("contactme.html", form=form, contact=contact)

@blueprint.route("/contact_me/")
def oldcontact():
	return redirect("/contact/", code=301)

@blueprint.route("/portfolio.html")
def oldportfolio():
	return redirect("/", code=301)

@blueprint.route("/contacts.html")
def oldestcontact():
	return redirect("/contact/", code=301)