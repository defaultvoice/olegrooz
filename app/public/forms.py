# -*- coding: utf-8 -*-

from flask_wtf import Form, RecaptchaField
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email


class ContactForm(Form):
	name = StringField("Name", validators=[DataRequired('Please enter your name.')])
	email = EmailField("Email", validators=[DataRequired("Please enter your email address."),
											Email("Please enter your email address.")])
	subject = StringField("Subject", validators=[DataRequired('Please enter subject.')])
	message = TextAreaField("Message", validators=[DataRequired('Please enter your message.')])
	submit = SubmitField("Send")

	def __init__(self, *args, **kwargs):
		super(ContactForm, self).__init__(*args, **kwargs)
		self.user = None

	def validate(self):
		initial_validation = super(ContactForm, self).validate()
		if not initial_validation:
			return False
		return True