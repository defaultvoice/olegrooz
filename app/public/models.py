# -*- coding: utf-8 -*-

import datetime
from app.extensions import db


class Post(db.Document):
	created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
	num = db.IntField(required=True)
	slug = db.IntField(required=True)
	title = db.StringField(max_length=255, required=True)
	videoSource = db.StringField(max_length=255, required=True)
	service = db.StringField(max_length=255, required=True)
	thumb = db.StringField(max_length=255, required=True)
	img = db.StringField(max_length=255, required=True)
	thumbTitle = db.StringField(max_length=255)
	description = db.StringField()
	tags = db.ListField()

	def __unicode__(self):
		return self.title

	meta = {
		'ordering': ['-num'],
		'indexes': ['num', 'slug', '-created_at'],
		'collection': 'posts'
	}

class Contacts(db.Document):
	img = db.StringField(max_length=255, required=True)
	img_width = db.DecimalField()
	body = db.StringField(required=True)
	vimeo = db.StringField(max_length=255)
	facebook = db.StringField(max_length=255)
	instagram = db.StringField(max_length=255)
	vk = db.StringField(max_length=255)
	main = db.StringField(max_length=255)
	meta = {
		'collection': 'contacts'
	}