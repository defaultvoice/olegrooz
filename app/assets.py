# -*- coding: utf-8 -*-
from flask_assets import Bundle, Environment

css = Bundle(
    "css/style.css",
    "admin/css/bootstrap.css",
    filters="cssmin",
    output="css/common.css"
)

js = Bundle(
    "js/plugin.js",
    filters="jsmin",
    output="js/common.js"
)

jsadmin = Bundle(
    "admin/js/jquery-ui.min.js",
    "libs/jquery.wysiwyg/jquery.wysiwyg.js",
    "libs/bootstrap/dist/js/bootstrap.min.js",
    "admin/js/plugin.js",
    filters="jsmin",
    output="admin/js/admin_common.js"
)

cssadmimn = Bundle(
    "admin/css/style.css",
    "admin/css/bootstrap.css",
    "libs/jquery.wysiwyg/jquery.wysiwyg.css",
    filters="cssmin",
    output="admin/css/admin_common.css"
)

assets = Environment()

assets.register("js_all", js)
assets.register("css_all", css)
assets.register("js_admin", jsadmin)
assets.register("css_admin", cssadmimn)
