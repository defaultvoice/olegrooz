#! venv/bin/python3.4
# -*- coding: utf-8 -*-
'''The app module, containing the app factory function.'''
from flask import Flask, render_template, redirect, url_for, flash
from werkzeug.contrib.fixers import ProxyFix

from app.settings import ProdConfig
from app.assets import assets
from app.extensions import (
	bcrypt,
	cache,
	db,
	login_manager,
	csrf,
	mail,
	allowed_file
)

from app import public, or_admin


def create_app(config_object=ProdConfig):
	"""An application factory, as explained here:
		http://flask.pocoo.org/docs/patterns/appfactories/

	:param config_object: The configuration object to use.
	"""
	app = Flask(__name__)
	app.config.from_object(config_object)
	app.wsgi_app = ProxyFix(app.wsgi_app)
	register_extensions(app)
	register_blueprints(app)
	register_errorhandlers(app)
	return app


def register_extensions(app):
	assets.init_app(app)
	bcrypt.init_app(app)
	cache.init_app(app)
	db.init_app(app)
	login_manager.init_app(app)
	csrf.init_app(app)
	mail.init_app(app)
	allowed_file.init_app(app)
	return None


def register_blueprints(app):
	app.register_blueprint(public.views.blueprint)
	app.register_blueprint(or_admin.views.blueprint)
	return None


def register_errorhandlers(app):
	def render_error(error):
		# If a HTTPException, pull the `code` attribute; default to 500
		error_code = getattr(error, 'code', 500)
		if error_code != 401:
			return render_template("{0}.html".format(error_code)), error_code
		flash('You are not authorized to see this page. Please, log in.', 'info')
		return redirect(url_for('or_admin.login'))

	for errcode in [401, 404, 500]:
		app.errorhandler(errcode)(render_error)
	return None
