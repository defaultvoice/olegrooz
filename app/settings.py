# -*- coding: utf-8 -*-
import os

os_env = os.environ


class Config(object):
	SECRET_KEY = "0hshiticant5w33m"
	APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
	PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
	BCRYPT_LOG_ROUNDS = 13
	ASSETS_DEBUG = False
	CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.
	MONGODB_SETTINGS = {'DB': "olegrooz"}
	RECAPTCHA_PUBLIC_KEY = "6Lf-CAcTAAAAACUY31twDQyIP8V9Tv-Ald7yKgXG"
	RECAPTCHA_PRIVATE_KEY = "6Lf-CAcTAAAAAOo2jyKKWRbSin3sAU1Lnqlvk-h7"
	RECAPTCHA_PARAMETERS = {'render': 'explicit'}
	RECAPTCHA_OPTIONS = dict(theme='dark')
	MAIL_SERVER = 'smtp.yandex.ru'
	MAIL_PORT = '465'
	MAIL_USE_SSL = True
	MAIL_USERNAME = 'noreply@olegrooz.com'
	MAIL_PASSWORD = 'skRvL3aeL'
	UPLOADED_FILES_DEST = APP_DIR + '/static/media'
	ALLOWED_EXTENSIONS = ['jpg', 'png', 'jpeg']
	MAX_CONTENT_LENGTH = 16 * 1024 * 1024


class ProdConfig(Config):
	"""Production configuration."""
	ENV = 'prod'
	DEBUG = True


class DevConfig(Config):
	"""Development configuration."""
	ENV = 'dev'
	DEBUG = True
	ASSETS_DEBUG = True  # Don't bundle/minify static assets


class TestConfig(Config):
	TESTING = True
	DEBUG = True
	BCRYPT_LOG_ROUNDS = 1  # For faster tests
	WTF_CSRF_ENABLED = False  # Allows form testing
