# -*- coding: utf-8 -*-
"""Extensions module. Each extension is initialized in the app factory located
in app.py
"""

from flask_bcrypt import Bcrypt
bcrypt = Bcrypt()

from .utils import AllowedFile
allowed_file = AllowedFile()

from flask_login import LoginManager
login_manager = LoginManager()

from flask_mongoengine import MongoEngine
db = MongoEngine()

from flask_cache import Cache
cache = Cache()

from flask_wtf.csrf import CsrfProtect
csrf = CsrfProtect()

from flask_mail import Mail
mail = Mail()
