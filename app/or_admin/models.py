# -*- coding: utf-8 -*-

import datetime
from app.extensions import db, bcrypt


class User(db.Document):
	username = db.StringField(max_length=255, required=True)
	password = db.StringField(required=True)
	created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
	email = db.StringField(max_length=255, required=True)

	def __init__(self, username, *args, **kwargs):
		super(User, self).__init__(username, *args, **kwargs)
		self.username = username

	def set_password(self, password):
		self.password = bcrypt.generate_password_hash(password)

	def check_password(self, value):
		return bcrypt.check_password_hash(self.password, value)

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return self.username

	meta = {
		'ordering': ['-created_at'],
		'indexes': ['username', '-created_at'],
		'collection': 'users'
	}
