# -*- coding: utf-8 -*-

from flask_wtf import Form
from wtforms import StringField, PasswordField, SubmitField, TextAreaField, SelectMultipleField, DecimalField
from flask_wtf.file import FileField
from wtforms.validators import DataRequired

from app.or_admin.models import User


class LoginForm(Form):
	username = StringField('Username', validators=[DataRequired()])
	password = PasswordField('Password', validators=[DataRequired()])
	submit = SubmitField("Send")

	def __init__(self, *args, **kwargs):
		super(LoginForm, self).__init__(*args, **kwargs)
		self.user = None

	def validate(self):
		initial_validation = super(LoginForm, self).validate()
		if not initial_validation:
			return False
		print('Start logging in')
		self.user = User.objects(username=self.username.data).first()
		if not self.user:
			print("Fail Check Login")
			self.username.errors.append('Unknown username or password')
			return False
		if not self.user.check_password(self.password.data):
			print("Fail Check Password")
			self.username.errors.append('Unknown username or password')
			return False
		print(self.username.data, self.user.check_password(self.password.data))
		return True


class PostForm(Form):
	thumb = FileField('Thumbnail')
	thumbTitle = StringField('Thumbnail Label', validators=[DataRequired()])
	title = StringField('Title of post', validators=[DataRequired()])
	img = FileField('Cover of video')
	videoSource = StringField('Video address', validators=[DataRequired()])
	description = TextAreaField('Description of post', validators=[DataRequired()])
	tags = SelectMultipleField('Tags', choices=[('music_videos', 'Music Videos'), ('live', 'Live'),
												('diaries', 'Diaries'), ('promo', 'Promo')])
	submit = SubmitField("Save")

	def validate(self):
		initial_validation = super(PostForm, self).validate()
		if not initial_validation:
			return False
		print('Starting upload files')
		return True


class ContactForm(Form):
	img = FileField('Photo')
	img_width = DecimalField('Photo width')
	body = TextAreaField('Text of contacts')
	vimeo = StringField('Vimeo')
	facebook = StringField('Facebook')
	instagram = StringField('Instagram')
	vk = StringField('VK')
	submit = SubmitField("Save")