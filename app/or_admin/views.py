# -*- coding: utf-8 -*-

import re
from datetime import datetime
from flask import (Blueprint, request, render_template, flash, url_for, redirect, session)
from flask.views import MethodView
from flask.ext.login import login_user, login_required, logout_user
from app.or_admin.forms import LoginForm, PostForm, ContactForm
from app.or_admin.models import User
from app.public.models import Post, Contacts
from app.extensions import csrf, login_manager, allowed_file
from app.utils import flash_errors

blueprint = Blueprint("or_admin", __name__, url_prefix='/or_admin', static_folder="../static")


@login_manager.user_loader
def load_user(username):
	return User.objects(username=username).first()


@blueprint.route("/")
@login_required
def home():
	username = session["user_id"]
	#	return render_template("admin/admin.html", username=username)
	return redirect(url_for('or_admin.posts'))


@csrf.exempt
@blueprint.route("/login/", methods=["GET", "POST"])
def login():
	form = LoginForm(request.form)
	if request.method == 'POST':
		if form.validate_on_submit():
			login_user(form.user)
			flash("You are logged in.", 'success')
			redirect_url = request.args.get("next") or url_for("or_admin.posts")
			return redirect(redirect_url)
		else:
			flash_errors(form, "danger")
		flash("You are logged in.", 'success')
	return render_template("admin/login.html", form=form)


@blueprint.route("/posts/")
@login_required
def posts():
	username = session["user_id"]
	posts = Post.objects.all()
	return render_template("admin/posts.html", posts=posts, username=username)


class Detail(MethodView):
	decorators = [login_required, csrf.exempt]

	def get_context(self, num=None):
		form_cls = PostForm

		if num:
			post = Post.objects.get_or_404(num=num)
			if request.method == 'POST':
				form = form_cls(request.form, inital=post._data)
			else:
				if post.service == 'youtube':
					post.videoSource = 'https://youtube.com/watch?v=' + post.videoSource
				else:
					post.videoSource = 'https://vimeo.com/' + post.videoSource
				form = form_cls(obj=post)
		else:
			post = Post()
			if Post.objects.order_by('-num').first():
				post.num = Post.objects.order_by('-num').first().num + 1
				post.slug = Post.objects.order_by('-slug').first().slug + 1
			else:
				post.num = 1
				post.slug = 1
			form = form_cls(request.form)

		context = {
			"post": post,
			"form": form,
			"create": num is None
		}
		return context

	def get(self, num):
		username = session["user_id"]
		context = self.get_context(num)
		return render_template('admin/detail.html', username=username, **context)

	def post(self, num):
		username = session["user_id"]
		context = self.get_context(num)
		form = context.get('form')
		if form.validate_on_submit():
			post = context.get('post')
			form.populate_obj(post)

			img = request.files['img']
			thumb = request.files['thumb']

			if img and allowed_file.get(img.filename):
				post.img = 'full-' + str(post.slug) + '.' + img.filename.rsplit('.', 1)[1]
				allowed_file.save_file(img, post.img)
			elif not context.get('create'):
				post.img = Post.objects.get_or_404(num=post.num).img
			else:
				flash_errors('Cover field: Image required!')
				return render_template('admin/detail.html', username=username, **context)

			if thumb and allowed_file.get(thumb.filename):
				post.thumb = 'thumb-' + str(post.slug) + '.' + thumb.filename.rsplit('.', 1)[1]
				allowed_file.save_file(thumb, post.thumb)
			elif not context.get('create'):
				post.thumb = Post.objects.get_or_404(num=post.num).thumb
			else:
				flash_errors('Thumbnail field: Image required!')
				return render_template('admin/detail.html', username=username, **context)

			if post.videoSource.find('youtube') != -1:
				post.service = 'youtube'
				source_pattern = re.compile('v=([\w/_-]*)&?')
				post.videoSource = source_pattern.findall(post.videoSource)[0]
			elif post.videoSource.find('vimeo') != -1:
				post.service = 'vimeo'
				source_pattern = re.compile('vimeo.com/(\d*)/?')
				post.videoSource = source_pattern.findall(post.videoSource)[0]
			else:
				flash_errors('Unknown video service!')
				return render_template('admin/detail.html', username=username, **context)
			post.save()

			return redirect(url_for('or_admin.posts'))
		else:
			flash_errors(form, "danger")
		return render_template('admin/detail.html', username=username, **context)


blueprint.add_url_rule('/posts/create/', defaults={'num': None}, view_func=Detail.as_view('create'))
blueprint.add_url_rule('/posts/<num>/', view_func=Detail.as_view('edit'))


@blueprint.route('/posts/del/<num>/')
@login_required
def delete(num):
	post = Post.objects(num=num).first()
	allowed_file.remove_file(post.img)
	allowed_file.remove_file(post.thumb)
	post.delete()
	flash('Post has ben deleted', 'info')
	return redirect(url_for('or_admin.posts'))


@blueprint.route('/contacts/', methods=["GET", "POST"])
@login_required
def contacts():
	username = session["user_id"]
	contacts = Contacts.objects.get_or_404(main='yep')
	form = ContactForm(obj=contacts)
	if request.method == 'POST':
		if form.validate_on_submit():
			form.populate_obj(contacts)
			img = request.files['img']
			if img:
				if allowed_file.get(img.filename):
					contacts.img = 'photo' + '.' + img.filename.rsplit('.', 1)[1]
					allowed_file.save_file(img, contacts.img)
				else:
					flash_errors('Photo field: File is not allowed!')
					return render_template('admin/contacts.html', form=form, username=username)
			else:
				contacts.img = Contacts.objects.get_or_404(main='yep').img
			contacts.main = 'yep'
			contacts.save()
			flash('Saved', category='info')
	return render_template('admin/contacts.html', form=form, username=username)


@blueprint.route('/logout/')
@login_required
def logout():
	logout_user()
	session.clear()
	flash('You are logged out.', 'info')
	return redirect(url_for('or_admin.contacts'))


@csrf.exempt
@blueprint.route('/sort/', methods=["POST"])
@login_required
def sort():
	data = request.get_json(force=True)
	for elem in data:
		post = Post.objects(slug=elem).first()
		post.num = data.get(elem)
		post.save()
	return 'All right'
