# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""
from flask import flash
from werkzeug.security import safe_str_cmp
import os
import bcrypt
from sys import version_info as PYVER

PYVER = PYVER[0]


def flash_errors(form, category="warning"):
	"""Flash all errors for a form."""
	for field, errors in form.errors.items():
		for error in errors:
			flash("{0}".format(error), category)


class AllowedFile(object):

	_allowed_extensions = ['jpg', 'png', 'mp3', 'mp4', 'avi']
	_destination = os.path.abspath(os.path.dirname(__file__)) + '/static/media'

	def __init__(self, app=None):
		if app is not None:
			self.init_app(app)

	def init_app(self, app):
		self._allowed_extensions = app.config.get('ALLOWED_EXTENSIONS')
		self._destination = app.config.get('UPLOADED_FILES_DEST')

	def get(self, filename):
		return '.' in filename and filename.rsplit('.', 1)[1] in self._allowed_extensions

	def save_file(self, file, filename):
		file.save(os.path.join(self._destination, filename))

	def remove_file(self, filename):
		os.remove(self._destination + '/' + filename)
		print('File has been removed')


class Bcrypt(object):
	"""TEMPORARY WORKAROUND! flask_bcrypt is not working with unicode strings"""
	_log_rounds = 12

	def __init__(self, app=None):
		if app is not None:
			self.init_app(app)

	def init_app(self, app):
		self._log_rounds = app.config.get('BCRYPT_LOG_ROUNDS', 12)

	def generate_password_hash(self, password, rounds=None):
		if not password:
			raise ValueError('Password must be non-empty.')

		if rounds is None:
			rounds = self._log_rounds

		if PYVER >= 3 and not isinstance(password, bytes):
			password = password.encode('ascii')
		return bcrypt.hashpw(password, bcrypt.gensalt(rounds))

	def check_password_hash(self, pw_hash, password):
		if PYVER >= 3 and not isinstance(password, bytes):
			password = password.encode('ascii')
		return safe_str_cmp(bcrypt.hashpw(password, pw_hash), pw_hash)