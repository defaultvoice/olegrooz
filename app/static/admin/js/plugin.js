$(function() {
    $(".alert").each(function(index, e) {
       setTimeout(
           $.proxy(
               function(){$(e).fadeOut('slow', function () { $(this).remove()})}, e
           ), 3000
       )
    });

    $( "#sortable" ).sortable({
        items: ".sort-item",
        connectWith: "#sortable",
        handle: ".sort-handler",
        stop: function( ) {
            var sortItems = $('.sort-item');
            for (var i = 0; i < sortItems.length; i++) {
                sortItems[i].dataset.num = sortItems.length-i;
            }
            setSort();
        }
    });

    $('#wysiwyg').wysiwyg({
        css: 'https://static.olegrooz.com/static/admin/css/editor.css',
        autoSave: true
    });

    $('textarea#body').wysiwyg({
        css: 'https://static.olegrooz.com/static/admin/css/editor.css',
        autoSave: true
    });

    function setSort() {
        var sort = {};
        $('.sort-item').each(function(indx){
            var slug = $(this).data('slug');
            var num = $(this).data('num');
            sort[slug] = num;
        });
        console.log(sort);
        $.ajax({
            url: '/or_admin/sort/',
            type: 'POST',
            processData: false,
            data: JSON.stringify(sort),
            contentType: 'application/json;charset=UTF-8',
            success: function(data){
                console.log(data)
            }
        });
    };
    console.info('Developer: defaultvoice');
    console.info('http://defaultvoice.ru');
});