/*
	SuperBox v1.0.0
	by Todd Motto: http://www.toddmotto.com
	Latest version: https://github.com/toddmotto/superbox
	
	Copyright 2013 Todd Motto
	Licensed under the MIT license
	http://www.opensource.org/licenses/mit-license.php

	SuperBox, the lightbox reimagined. Fully responsive HTML5 image galleries.
*/
;(function($) {

    $(".alert").each(function(index, e) {
       setTimeout(
           $.proxy(
               function(){$(e).fadeOut('slow', function () { $(this).remove()})}, e
           ), 3000
       )
    });

	$.fn.sticky = function(){
	    var target = jQuery(this);
	    jQuery(target).each(function(){
	    jQuery(this)
	        .data('initial-offset',jQuery(this).offset().top)
	        .on('stickyScroll',function(e,winOffset){
	            if((winOffset > jQuery(this).data('initial-offset'))){
	                if(!jQuery(this).is('.bound')){
	                    jQuery(this).addClass('bound');
	                }
	            } else if(jQuery(this).hasClass('bound')){
	                jQuery(this).removeClass('bound');
	            }
	        });
	    });
	    jQuery(window).on('scroll', function(e){ jQuery(target).trigger('stickyScroll',jQuery(window).scrollTop());});
	};

	$.fn.SuperBox = function(options) {

		var superbox      = $('<div class="superbox-show"></div>');
		var superboxclose = $('<div class="superbox-close"></div>');
		var windowHeight = $(window).height();
		var imgHeight = $(window).height() - $(window).height()/100*20;
		var superbox      = $('<div class="superbox-show" style="min-height:'+ windowHeight +'px"></div>');
		var superboximg   = $('<div class="superbox-image-wrap"><img src="" style="max-height:'+ imgHeight +'px" class="superbox-current-img"><div class="play"><span class="glyphicon glyphicon-play-circle"></span></div></div>');
		var superboxName = $('<div class="superbox-name"></div>');
		var superboxDescription = $('<div class="superbox-description"></div>');
		superbox.append(superboxName).append(superboximg).append(superboxDescription).append(superboxclose);

		return this.each(function() {

			$('.superbox-list').click(function() {

				var currentimg = $(this).find('.superbox-img'),
					imgData = currentimg.data('img'),
					imgName = currentimg.attr('alt'),
                    imgThumbTitle = currentimg.data('thumbtitle'),
					imgDescription = currentimg.data('description');
				superboximg.find('img').attr('src', imgData);
				superboxDescription.html(imgDescription);
				superboxName.html(imgName + '<div>' + imgThumbTitle + '</div>');


				if($('.superbox-current-img').css('opacity') == 0) {
					$('.superbox-current-img').animate({opacity: 1});
				}

				if($('div').is('.iframe_wrap')) {
					$('.iframe_wrap').replaceWith(superboximg);
                    $('.superbox-image-wrap').find('div').show()
				}
				
				if ($(this).next().hasClass('superbox-show')) {
					superbox.toggle();
				} else {
					superbox.insertAfter(this).css('display', 'block');
				}
				
				$('html, body').animate({
					scrollTop: superbox.position().top + 1
				}, 'medium');
			
			});

			$('.superbox').on('click', '.superbox-close', function() {
				$('.superbox-current-img').animate({opacity: 0}, 300, function() {
					$('.superbox-show').slideUp();
				});
				$('.video').animate({opacity: 0}, 300, function() {
					$('.superbox-show').slideUp();
                    $(this).remove();
				});
			});

			$('.superbox').on('click', '.superbox-image-wrap', function() {
				var img = $(this).find('img'),
					videoSource = $(this).parent().prev().find('img').data('source'),
                    service = $(this).parent().prev().find('img').data('service'),
					width = img.width(),
					height = img.height(),
                    div = $(this).find('div');
				console.log(videoSource);
                div.hide()
                if (service == 'youtube') {
				    $('.superbox-image-wrap').replaceWith('<div class="iframe_wrap"><iframe class="video" width="' + width + '" height="' + height + '" src="https://www.youtube.com/embed/'+ videoSource +'?autoplay=1" frameborder="0" allowfullscreen></iframe></div>');
                } else {
                    $('.superbox-image-wrap').replaceWith('<div class="iframe_wrap"><iframe class="video" src="https://player.vimeo.com/video/'+ videoSource +'?autoplay=1&color=ffffff&byline=0&portrait=0" width="' + width + '" height="' + height + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>');
                }
			});
			
		});
	};

    console.info('Developer: defaultvoice');
    console.info('http://defaultvoice.ru');

})(jQuery);