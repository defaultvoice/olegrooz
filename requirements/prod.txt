# Everything needed

# Flask
Flask==0.10.1
MarkupSafe==0.23
Werkzeug==0.10.4
Jinja2==2.7.3
itsdangerous==0.24

# WTForms
flask-wtf==0.11
WTForms==2.0.2

# MongoDB
pymongo==2.8
mongoengine==0.8.7
flask-mongoengine==0.7.1

# Caching
Flask-Cache>=0.13.1

# Auth
Flask-Login==0.2.11
bcrypt==1.1.1
Flask-Bcrypt==0.6.2

# Manage
Flask-Script==2.0.5

# Assets
Flask-Assets==0.10
cssmin==0.2.0
jsmin==2.1.1

# Mail
blinker==1.3
flask-mail==0.9.1